# Eye of Sauron

This is a school project, made by three students in the second year (SPE) at
EPITA Strasbourg. It is a character recognition program, on the command line.


## Requirements

You will only need one thing to get you up and running: SDL1.2. If you want to
use the GUI (booh, who uses GUIs), you will also need to have Gtk+3 installed.
To do so, use your distro's packet manager, or refer to the documentation of
those two librairies (RTFM ¯\\_(ツ)_/¯).


## Quick Start

To get you up and running quickly:
```
git clone --depth=1 https://gitlab.com/42-2/eyeofsauron.git
cd eyeofsauron
make
make install
cd ..
rm -rf eyeofsauron
eyeofsauron path/to/image
```


## Usage

To analyse a simple image: `eyeofsauron path/to/image`. Output will go to
standard output unless `-o output-file` or `--output=output-file` is passed.

Image format supported: BMP, PNM (PPM/PGM/PBM), XPM, LBM, PCX, GIF, JPEG, PNG,
TGA, TIFF


## Documentation

There are two types of documentation. A doc that we call _doxydocs_, that
more or less, lists our functions and briefly explains them. Then there are
our first defense report and our project report, as this is a school project.
Here's how you can get them :

* doxydocs: `make doxydocs` and go see in the directory `doxydocs` for a LaTeX
  and a HTML version of the docs
* defense and project reports: `make docs` and two pdfs will magically appear
  before your eyes.
* all of the above: `make alldocs`


## Contributing

Any contributions, whether to our existing code or as separate applications,
are very welcome!

Thank you for you contributions!


## Bug report

If you find a bug in this program, please open an issue
[here](https://gitlab.com/42-2/eyeofsauron/issues).


## License

See [LICENSE.md](LICENSE.md) for details.
