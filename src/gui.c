/**
 * @file gui.c
 * @brief Manage our GUI
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <gtk/gtk.h>
#include "image.h"

typedef struct
{
  GtkBuilder *builder;
  gpointer user_data;
} SGlobalData;

void callback_about(GtkMenuItem *menuitem, gpointer user_data)
{
  (void)menuitem;
  SGlobalData *data = (SGlobalData *) user_data;
  GtkWidget *dialog = NULL;
  dialog =  GTK_WIDGET (gtk_builder_get_object (data->builder, "AboutWindow"));
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_hide (dialog);
}

void callback_process(GtkFileChooserButton *widget, gpointer user_data)
{
  (void) user_data;
  GFile *chosen_file = gtk_file_chooser_get_file(GTK_FILE_CHOOSER(widget));
  char *file = g_file_get_path(chosen_file);
  int exit_code;
  char *result = process_image(file, 1, &exit_code);

  if (exit_code != 0)
    {
      gtk_main_quit();
    }

  GtkWidget *outputText = gtk_text_view_new();
  GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(outputText));
  gtk_text_buffer_set_text (buffer, result, -1);
}

int start_gui(int argc, char *argv[])
{
  GtkWidget *main_window = NULL;
  SGlobalData data;
  GError *error = NULL;
  gchar *filename = NULL;
  gtk_init(&argc, &argv);
  data.builder = gtk_builder_new();
  filename = g_build_filename ("eyeofsauron.glade", NULL);
  gtk_builder_add_from_file (data.builder, filename, &error);
  g_free (filename);

  if (error)
    {
      gint code = error->code;
      g_printerr("%s\n", error->message);
      g_error_free (error);
      return code;
    }

  main_window = GTK_WIDGET(gtk_builder_get_object(data.builder,
                           "main_window"));
  gtk_window_set_title(GTK_WINDOW(main_window), "Eye of Sauron");
  gtk_builder_connect_signals (data.builder, &data);
  gtk_widget_show_all(main_window);
  gtk_main();
  return 0;
}
