/**
 * @file output.c
 * @brief Handles the output of the text recognized by the neural network
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int output(char *text, char *path, int verbose)
{
  (void)verbose;
  int exit_code = 0;

  if (strcmp(path, "-") == 0) // Strings are equal
    {
      printf("%s", text);
      exit_code = 0;
    }
  else
    {
      FILE *fp = fopen(path, "w");

      if (fp == NULL)
        {
          exit_code = 2;
        }
      else
        {
          fputs(text, fp);
          fclose(fp);
          exit_code = 0;
        }
    }

  return exit_code;
}
