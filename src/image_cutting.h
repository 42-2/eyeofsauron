/**
 * @file image_cutting.h
 * @brief Header file for image_cutting.c
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include "image.h"
#include "perceptron.h"
#ifndef IMAGE_CUTTING_H
#define IMAGE_CUTTING_H

typedef struct char_line_position char_line_position;
typedef struct character character;

/**
 * Structure containing first and last pixel lines of a line of character. Can
 * also contain the number of line of the input image.
 */
struct char_line_position
{
  size_t first_pixel_line; /**!< Number of the first pixel line from
                                         the top */
  size_t last_pixel_line; /**!< Number of the last pixel line from the
                                         top */
  size_t nb_lines; /**!< Number of lines of characters in the image*/
};

/**
 * Structure containing first and last pixel lines and columns of a character.
 * Can also contain the number of character of the input image.
 */
struct character
{
  size_t first_pixel_line;/**!< Number of the first pixel line from
                                         the top */
  size_t last_pixel_line; /**!< Number of the last pixel line from the
                                         top */
  size_t first_pixel_column; /**!< Number of the first pixel column
                                    from the top */
  size_t last_pixel_column; /**!< Number of the last pixel column
                                    from the top */
  char character_matrix[INPUT_NUMBER]; /**!< 28x28 matrix representing the
                                         character using 0s and 1s */
  unsigned char character; /**!< The character recognized by the neural
                              network */
};

character *cut_image(image img, character *chs, size_t *nb_char, int verbose,
                     int *exit_code);
char *chs_to_string(character *chs, size_t nb_char, int *exit_code);

#endif
