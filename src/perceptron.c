#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "image.h"
#include "import.h"
#include "perceptron.h"
#include "image_cutting.h"
#include "preprocessing.h"
#define ABS(a) ((a<0)?(-a):(a))
/**
 * Creates a neural network with random weigts in a Perceptron structure
 * @return a Perceptron structure with random weights
 */
Perceptron init()
{
  srand(time(NULL));
  Perceptron percept;

  for (int i = 0; i < INPUT_NUMBER; i++)
    {
      for (int j = 0; j < HIDDEN_NUMBER; j++)
        {
          percept.weights1[i][j] = 2 * (((float) rand()) / RAND_MAX) - 1;
        }
    }

  for (int i = 0; i < HIDDEN_NUMBER; i++)
    {
      for (int j = 0; j < OUTPUT_NUMBER; j++)
        {
          percept.weights2[i][j] = 2 * (((float) rand()) / RAND_MAX) - 1;
        }
    }

  percept.bias = 1;
  return percept;
}

void getTargetInput(char path[], float targetValues[])
{
  for (int i = 0; i < OUTPUT_NUMBER; i++)
    {
      targetValues[i] = 0;
    }

  size_t n = strlen(path);
  size_t ascii_code = (path[n - 11] - 48) + (path[n - 12] - 48) * 10 +
                      (path[n - 13] - 48) * 100;

  if (ascii_code - 33 < OUTPUT_NUMBER)
    {
      targetValues[ascii_code - 33] = 1;
    }
}

char **getTrainingFiles(char path[], size_t *nb_files)
{
  char **paths;
  DIR *d;
  struct dirent *dir;
  d = opendir(path);

  if (d)
    {
      *nb_files = 0;

      while ((dir = readdir(d)) != NULL)
        {
          if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
            {
              continue;
            }

          ++(*nb_files);
        }

      closedir(d);
    }

  d = opendir(path);

  if (d)
    {
      paths = malloc((*nb_files) * sizeof(char *));
      size_t i = 0;
      size_t path_length = 0;

      while ((dir = readdir(d)) != NULL)
        {
          if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
            {
              continue;
            }

          path_length = strlen(path) + strlen(dir->d_name) + 1;
          paths[i] = malloc(path_length * sizeof(char));
          snprintf(paths[i], path_length, "%s%s", path, dir->d_name);
          ++i;
        }

      closedir(d);
    }

  return paths;
}

TrainItem *getTrainingData(size_t *nb_item)
{
  char path[] = "../dataset/";
  char **paths;
  image img;
  character *inputs = NULL;
  size_t nb_files = 0;
  paths = getTrainingFiles(path, &nb_files);
  TrainItem *items = malloc(nb_files * sizeof(TrainItem));
  *nb_item = nb_files;
  size_t c = 0;
  size_t placeholder;
  int exit_code = 0;

  for (size_t count = 0; count < nb_files; ++count)
    {
      c = 0;
      placeholder = 0;

      if (import_img(paths[count], &img, 0) != 0)
        {
          continue;
        }

      getTargetInput(paths[count], items[count].targetedOutput);
      preprocess(&img, 0);
      inputs = cut_image(img, inputs, &placeholder, 0, &exit_code);

      if (exit_code != 0)
        {
          continue;
        }

      while (inputs[c].character != '\0' && c < placeholder)
        {
          ++c;
        }

      for (int i = 0; i < INPUT_NUMBER; i++)
        {
          items[count].pixelValues[i] = inputs[c].character_matrix[i];
        }

      free(paths[count]);
      destroy_img(img);
    }

  free(paths);
  return items;
}

/**
 * Trains a neural network to recognise characters in the dataset folder
 * @param int n the number of trainings
 */
void train(int n)
{
  /* Initiate a Perceptron structure */
  Perceptron percept = init();
  size_t nb_inputs;
  /* Loads training files */
  TrainItem *inputCharacters = getTrainingData(&nb_inputs);
  printf("Training...\n");
  int count = 0;
  float sumerror = 0;

  /* For each loaded matrix, run a feedforward and a backpropagation */
  for (int i = 0 ; i < n; i++)
    {
      sumerror = 0;

      for (size_t j = 0; j < nb_inputs; j++)
        {
          for (int k = 0; k < INPUT_NUMBER; k++)
            {
              percept.inputData[k] = inputCharacters[j].pixelValues[k];
            }

          percept = feedForward(percept);
          percept = backPropagation(percept,
                                    inputCharacters[j].targetedOutput, &sumerror);
          count++;
        }

      printf("total error : %f\n", sumerror);
      printf("next training: %d\n", i + 1);
    }

  printf("Done: trained on %d files\n", count);
  free(inputCharacters);
  /* Save weights */
  FILE *saveFile = fopen("eyeofsauron.w", "w");

  if (saveFile != NULL)
    {
      for (int i = 0; i < INPUT_NUMBER; i++)
        {
          for (int j = 0; j < HIDDEN_NUMBER; j++)
            {
              fprintf(saveFile, "%f\n", percept.weights1[i][j]);
            }
        }

      for (int i = 0; i < HIDDEN_NUMBER; i++)
        {
          for (int j = 0; j < OUTPUT_NUMBER; j++)
            {
              fprintf(saveFile, "%f\n", percept.weights2[i][j]);
            }
        }

      fclose(saveFile);
    }
  else
    {
      printf("Failed saving the weights");
    }
}

/**
 * Runs the neural network to recognise characters
 * @param character* characters the list of character to recognise
 * @param size_t nb_char the number of characters to recognise
 * @param int verbose whether to be verbose
 * @return the characters with recognised values
 */
character *run(character *characters, size_t nb_char, int verbose,
               int *exit_code)
{
  if (verbose)
    {
      printf("\nAnalysing the image");
    }

  Perceptron percept = init();
  unsigned char letter;
  FILE *weights = fopen("eyeofsauron.w", "r");
  char sweights[30];

  if (verbose)
    {
      printf("\nLoading weights");
    }

  /* Load weights from file */
  if (weights != NULL)
    {
      for (int i = 0; i < INPUT_NUMBER; i++)
        {
          for (int j = 0; j < HIDDEN_NUMBER; j++)
            {
              if (fgets(sweights, 30, weights) == NULL)
                {
                  *exit_code = 420;
                  return characters;
                }

              percept.weights1[i][j] = atof(sweights);
            }
        }

      for (int i = 0; i < HIDDEN_NUMBER; i++)
        {
          for (int j = 0; j < OUTPUT_NUMBER; j++)
            {
              if (fgets(sweights, 30, weights) == NULL)
                {
                  *exit_code = 420;
                  return characters;
                }

              percept.weights2[i][j] = atof(sweights);
            }
        }
    }
  else
    {
      *exit_code = 421;
      return characters;
    }

  if (verbose)
    {
      printf("\nRunning neural network");
    }

  /* run every character in the neural network */
  for (unsigned int i = 0; i < nb_char; ++i)
    {
      if (characters[i].character != '\0')
        {
          continue;
        }

      for (int j = 0; j < INPUT_NUMBER; j++)
        {
          percept.inputData[j] = characters[i].character_matrix[j];
        }

      percept = feedForward(percept);
      letter = getLetter(percept);
      characters[i].character = letter;
    }

  if (verbose)
    {
      printf("\nResult of the analysis:\n");
    }

  return characters;
}
/** Determine the letter recognised from the outputs of the neural network
 * @param Perceptron percept the network from which the outputs must be looked
 * @return the character recognised by the neural network
 */
unsigned char getLetter(Perceptron percept)
{
  int max = 0;

  for (int i = 1; i < OUTPUT_NUMBER; i++)
    {
      if (percept.outputData[1][max] < percept.outputData[1][i])
        {
          max = i;
        }
    }

  return (unsigned char)(max + 33);
}

/**
 * The sigmoid function
 * @param float x : the antecedent
 * @return the result of the application of sigmoid on the antecedent x
 */
float sigmoid(float x)
{
  return 1 / (1 + exp((double) - x));
}

/**
 * Does a feed forward through a neural network
 * @param Perceptron percept : the neural network to operate
 * @return the perceptron after feed forward (the ouput layer will contain
 * the output computed from the input
 */
Perceptron feedForward(Perceptron percept)
{
  float sum = 0;

  /* Input to hidden layer */
  for (int i = 0; i < HIDDEN_NUMBER; i++)
    {
      sum = 0;

      for (int j = 0; j < INPUT_NUMBER; j++)
        {
          sum += percept.inputData[j] * percept.weights1[j][i];
        }

      /* Putting in and out value of each hidden neuron */
      percept.hiddenData[0][i] = sum + percept.bias;
      percept.hiddenData[1][i] = sigmoid(sum);
    }

  /* Hidden layer to output */
  for (int i = 0; i < OUTPUT_NUMBER; i++)
    {
      sum = 0;

      for (int j = 0; j < HIDDEN_NUMBER; j++)
        {
          sum += percept.hiddenData[1][j] * percept.weights2[j][i];
        }

      /* Same with output */
      percept.outputData[0][i] = sum + percept.bias;
      percept.outputData[1][i] = sigmoid(sum);
    }

  return percept;
}

/**
 * Does a backpropagation through a neural network and updates the weights
 * @param Perceptron percept : the neural network in which the backpropagation
 * must be done
 * @param float *target : the values targeted (error will be calculated in
 * function of it and the output of the network
 * @param float *sumerror a pointer to a variable where to put the total error
 * after each training
 * @return the perceptron with updated weights
 */
Perceptron backPropagation(Perceptron percept, float *target, float *sumerror)
{
  float alpha = 0.2;
  /* Calculating output error */
  float error[OUTPUT_NUMBER];

  for (int i = 0; i < OUTPUT_NUMBER; i++)
    error[i] = sigmoid(percept.outputData[0][i]) * (1 - sigmoid(
                 percept.outputData[0][i])) * (percept.outputData[1][i]
                     - target[i]);

  for (int i = 0; i < OUTPUT_NUMBER; i++)
    {
      *sumerror += ABS(error[i]);
    }

  /* Updating weights between hidden and output layers */
  for (int i = 0; i < OUTPUT_NUMBER; i++)
    {
      for (int j = 0; j < HIDDEN_NUMBER; j++)
        percept.weights2[j][i] = percept.weights2[j][i] - alpha *
                                 percept.hiddenData[1][j] * error[i];
    }

  /* Calculating error value in hidden layer */
  float hiddenLayerError[HIDDEN_NUMBER];

  for (int i = 0; i < HIDDEN_NUMBER; i++)
    {
      hiddenLayerError[i] = 0;

      for (int j = 0; j < OUTPUT_NUMBER; j++)
        {
          hiddenLayerError[i] += error[i] * percept.weights2[i][j];
        }

      hiddenLayerError[i] = sigmoid(percept.hiddenData[0][i]) * (1 - sigmoid(
                              percept.hiddenData[0][i])) *
                            (hiddenLayerError[i]);
    }

  /* Updating weights between input and hidden layers */
  for (int i = 0; i < HIDDEN_NUMBER; i++)
    {
      for (int j = 0; j < INPUT_NUMBER; j++)
        {
          percept.weights1[j][i] = percept.weights1[j][i] - alpha *
                                   percept.inputData[j] *
                                   hiddenLayerError[i];
        }
    }

  return percept;
}
