/**
 * @file image.c
 * @brief Used to manipulate image structures
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stdlib.h>
#include "image.h"
#include "import.h"
#include "preprocessing.h"
#include "image_cutting.h"
#include "perceptron.h"

/**
 * Initialize a struct image
 * @param new_img a pointer to the image object where the image will be stored,
 * all variables are initialized at the right value, except pixels which
 * contains things
 * @param path the path to where the original image file is stored
 * @param height the height of the image in pixels
 * @param width the width of the image in pixels
 * @return the exit code of the function
 */
int init_img(image *new_img, char *path, size_t height, size_t width)
{
  /* Image properties */
  new_img->path = path;
  new_img->width = width;
  new_img->height = height;
  new_img->size = width * height;
  /* Allocating space to store the main data */
  new_img->pixels = malloc(new_img->height * sizeof(pixel *));

  if (new_img->pixels == NULL)
    {
      return 12;
    }

  for (size_t i = 0; i < new_img->height; i++)
    {
      new_img->pixels[i] = malloc(new_img->width * sizeof(pixel));

      if (new_img->pixels[i] == NULL)
        {
          return 12;
        }
    }

  return 0;
}

/**
 * Copy a struct image
 * @param img the image to copy
 * @param copy a pointer to the image where the copy will be stored
 * @return the exit code of the function
 */
int copy_img(image img, image *copy)
{
  int exit_code;
  /* First, we create a new image */
  exit_code = init_img(copy, img.path, img.height, img.width);

  if (exit_code != 0)
    {
      return exit_code;
    }

  /* Then we copy the data */
  for (size_t i = 0; i < img.height; i++)
    {
      for (size_t j = 0; j < img.width; j++)
        {
          copy->pixels[i][j] = img.pixels[i][j];
        }
    }

  return exit_code;
}

/**
 * Destroy a struct image
 * Frees arrays used in the image struct
 * @param img the image to destroy
 */
void destroy_img(image img)
{
  /* I'm free, da ba dee, da ba da */
  for (size_t i = 0; i < img.height; i++)
    {
      free(img.pixels[i]);
    }

  free(img.pixels);
}

/**
 * Process an image to get the text inside it
 * @param path the path from where to import the image
 * @param verbose whether to be verbose
 * @param exit_code a pointer whose value will contain the exit code of the
 * function
 * @return the text found in the image
 */
char *process_image(char *path, int verbose, int *exit_code)
{
  image img;
  character *chs = NULL;
  size_t nb_char;
  char *text;

  /* Initialize the image */
  if ((*exit_code = import_img(path, &img, verbose)) != 0)
    {
      if (*exit_code != 2)
        {
          destroy_img(img);
        }

      return NULL;
    }

  /* Preprocess the image */
  preprocess(&img, verbose);
  /* Find and cut the characters inside it */
  chs = cut_image(img, chs, &nb_char, verbose, exit_code);

  if (*exit_code != 0)
    {
      destroy_img(img);
      free(chs);
      return NULL;
    }

  destroy_img(img);/* Not needed anymore */
  /* Run the neural network to find the characters inside the image */
  chs = run(chs, nb_char, verbose, exit_code);

  if (*exit_code != 0)
    {
      free(chs);
      return NULL;
    }

  /* Extract the text */
  text = chs_to_string(chs, nb_char, exit_code);

  if (*exit_code != 0)
    {
      free(chs);
      return NULL;
    }

  free(chs);
  return text;
}
