/**
 * @file eyeofsauron.c
 * @brief TODO: brief description
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <limits.h>
#include "image.h"
#include "import.h"
#include "preprocessing.h"
#include "image_cutting.h"
#include "perceptron.h"
#include "output.h"
#include "gui.h"

/* Program version and bug address */
const char *argp_program_version = "eyeofsauron 0.21";
const char *argp_program_bug_address =
  "https://gitlab.com/42-2/eyeofsauron/issues";

/* Program documentation */
static char doc[] = "eyeofsauron -- an OCR program";

/* The arguments we accept */
static char args_doc[] = "input-image";

/* The options we understand */
static struct argp_option options[] =
{
  {"verbose", 'v', 0, 0, "Produce verbose output", 2},
  {"gui", 'g', 0, 0, "Launch the GUI", 3},
  {"train", 'T', 0, 0, "Train the neural network", 4},
  {
    "output", 'o', "output-file", 0,
    "Output to FILE instead of standard output", 1
  },
  {0}
};

/* Used by main to communicate with parse_opt */
struct arguments
{
  char *args[1];  /* INPUT-FILE */
  int verbose, train, gui;
  char *output_file;
};

/**
 * Parse a single option
 */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we know is a pointer to our
   * arguments structure */
  struct arguments *arguments = state->input;

  switch (key)
    {
      case 'T':
        arguments->train = 1;
        break;

      case 'g':
        arguments->gui = 1;
        break;

      case 'v':
        arguments->verbose = 1;
        break;

      case 'o':
        arguments->output_file = arg;
        break;

      case ARGP_KEY_ARG:
        if (state->arg_num >= 1)
          /** Too many arguments */
          {
            argp_usage (state);
          }

        arguments->args[state->arg_num] = arg;
        break;

      case ARGP_KEY_END:
        if (state->arg_num < 1)
          /** Not enough arguments */
          {
            argp_usage(state);
          }

        break;

      default:
        return ARGP_ERR_UNKNOWN;
    }

  return 0;
}

/** Our argp parser */
static struct argp argp =
{
  options, parse_opt, args_doc, doc, NULL, NULL, NULL
};

/**
 * README: workings of the image and pixel structures:
 * https://hackmd.io/s/HkkQyq-FQ
 */

void process_exit_code(int exit_code)
{
  switch (exit_code)
    {
      case 0:
        break;

      case 2:
        printf("\nNo such file.");
        break;

      case 12:
        printf("\nOut of memory.");
        break;

      case 42:
        printf("\nNo characters were found in this image.");
        break;
      case 420:
        printf("\nNot enough weights in the file.");
        break;
      case 421:
        printf("\nUnable to find weights file.");
        break;
      default:
        printf("\nAn unknown error occured.");
    }
}



/**
 * Where everything starts...
 */
int main(int argc, char *argv[])
{
  int exit_code;
  struct arguments args;
  /** Default values */
  args.verbose = 0;
  args.train = 0;
  args.gui = 0;
  args.output_file = "-";
  /* Parse our arguments; every option seen by parse_opt will be reflected in
   * arguments */
  argp_parse(&argp, argc, argv, 0, 0, &args);

  if (args.train) /* We train our neural network */
    {
#define NB_TRAIN 300 /* The number of training of the neural network */
      train(NB_TRAIN);
      return 0;
    }

  if (args.gui) /* We launch the gui */
    {
      exit_code = start_gui(argc, argv);
    }
  else /* Or we don't */
    {
      /* Process the image to get the text that is in it */
      char *text = process_image(args.args[0], args.verbose, &exit_code);

      if (exit_code == 0)
        {
          /* Everything is working out, we can output the text */
          exit_code = output(text, args.output_file, args.verbose);
        }

      /* Display useful messages if shit has gone sideways */
      process_exit_code(exit_code);
    }

  return exit_code;
}
