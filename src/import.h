/**
 * @file import.h
 * @brief Header file for import.h
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#ifndef IMPORT_H
#define IMPORT_H

int import_img(char *path, image *img, int verbose);

#endif
