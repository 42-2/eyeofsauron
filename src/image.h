/**
 * @file image.h
 * @brief Header file for image.c
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stddef.h>
#ifndef IMAGE_H
#define IMAGE_H

#define MIN_PIXEL_VAL 0
#define MID_PIXEL_VAL 140
#define MAX_PIXEL_VAL 255

typedef struct image image;
typedef struct pixel pixel;

/** Structure to contain the components red, green and blue of a pixel */
struct pixel
{
  unsigned char r; /**!< Red component of the pixel */
  unsigned char g; /**!< Green component of the pixel */
  unsigned char b; /**!< Blue component of the pixel */
};

/** Structure to contain an image */
struct image
{
  char *path; /**!< Path from where the image was imported */
  size_t width; /**!< The width of the image */
  size_t height; /**!< The height of the image */
  size_t size; /**!< The number of pixels (height * width) */
  pixel **pixels; /**!< The pixels contained in a two-dimensional array */
};

int init_img(image *new_img, char *path, size_t height, size_t width);
int copy_img(image img, image *copy);
void destroy_img(image img);
char *process_image(char *path, int verbose, int *exit_code);

#endif
