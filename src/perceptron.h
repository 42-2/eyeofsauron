#ifndef PERCEPTRON_H
#define PERCEPTRON_H
#define CHARACTER_SIZE 28
#define INPUT_NUMBER CHARACTER_SIZE * CHARACTER_SIZE
#define HIDDEN_NUMBER 110
#define OUTPUT_NUMBER 94
#define SAMPLE_NUMBER 1016
#include "image_cutting.h"

typedef struct Perceptron Perceptron;

/** Structure that represents a neural network*/
struct Perceptron
{
  float weights1[INPUT_NUMBER][HIDDEN_NUMBER]; /**!< weights between input and
                                                 hidden layers */
  float weights2[HIDDEN_NUMBER][OUTPUT_NUMBER]; /**!< weights between hidden
                                                  and output layers */
  float inputData[INPUT_NUMBER]; /**!< value of input layer neurons*/
  float hiddenData[2][HIDDEN_NUMBER]; /**!< values of hidden layer neurons */
  float outputData[2][OUTPUT_NUMBER]; /**!< values of output layer neurons */
  float bias; /**!< bias*/
};

typedef struct TrainItem TrainItem;
/** Structure storing a train item for the neural network */
struct TrainItem
{
  char pixelValues[INPUT_NUMBER]; /**!< pixel values of the input character to
                                    recognise */
  float targetedOutput[OUTPUT_NUMBER]; /**!< targeted output of the neural
                                         network */
};

void train(int n);
character *run(character *characters, size_t nb_char, int verbose,
               int *exit_code);
float sigmoid(float x);
Perceptron feedForward(Perceptron percept);
Perceptron backPropagation(Perceptron percept, float target[], float *sumerror);
unsigned char getLetter(Perceptron percept);
#endif
