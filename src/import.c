/**
 * @file import.c
 * @brief Import an image of various file formats and stores it in a struct
 * image @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "image.h"

/**
 * Retrieve a pixel from a SDL_Surface
 * Function provided by the SDL documentation
 * @param surface the surface from which we want to retrieve the image
 * @param x the x coordinate of the pixel
 * @param y the y coordinate of the pixel
 * @return an 8 bytes int containing the color informations of the pixel
 */
unsigned int get_pixel(SDL_Surface *surface, int x, int y)
{
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to retrieve */
  unsigned char *p = (unsigned char *)surface->pixels + y * surface->pitch + x
                     * bpp;

  switch (bpp)
    {
      case 1:
        return *p;

      case 2:
        return *(unsigned short *)p;

      case 3:
        if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
          {
            return p[0] << 16 | p[1] << 8 | p[2];
          }
        else
          {
            return p[0] | p[1] << 8 | p[2] << 16;
          }

      case 4:
        return *(unsigned int *)p;

      default:
        return 0;  /** shouldn't happen, but avoids warnings */
    }
}

/**
 * Import an image
 * Image types supported: BMP, PNM (PPM/PGM/PBM), XPM, LBM, PCX, GIF, JPEG,
 * PNG, TGA, TIFF
 * @param path the path to the image to import
 * @param img a pointer to the image object where the imported image will be
 * stored
 * @param verbose whether to be verbose
 * @return the exit code of the function
 */
int import_img(char *path, image *img, int verbose)
{
  if (verbose)
    {
      printf("\nImporting image from path: %s", path);
    }

  SDL_Surface *sdl_img;
  int exit_code;
  /* Import as an SDL surface */
  sdl_img = IMG_Load(path);

  if (!sdl_img)
    {
      return 2;
    }

  /* Initialize our image */
  exit_code = init_img(img, path, sdl_img->h, sdl_img->w);

  if (exit_code != 0)
    {
      return exit_code;
    }

  /* Fill all its pixels */
  for (size_t i = 0; i < (size_t) sdl_img->h; i++)
    {
      for (size_t j = 0; j < (size_t) sdl_img->w; j++)
        {
          unsigned char r, g, b;
          unsigned int sdl_pixel = get_pixel(sdl_img, j, i);
          SDL_GetRGB(sdl_pixel, sdl_img->format, &r, &g, &b);
          pixel current_pixel = {r, g, b};
          img->pixels[i][j] = current_pixel;
        }
    }

  if (verbose)
    {
      printf("\nImage: width: %ld, height: %ld, size: %ld", img->width,
             img->height, img->size);
      printf("\nImage successfully imported from %s", path);
    }

  return 0;
}
