/**
 * @file output.h
 * @brief Header file for output.h
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#ifndef OUTPUT_H
#define OUTPUT_H

int output(char *text, char *path, int verbose);

#endif
