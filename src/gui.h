/**
 * @file gui.h
 * @brief Header file for gui.c
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#ifndef GUI_H
#define GUI_H
#include <gtk/gtk.h>

int start_gui(int argc, char *argv[]);

#endif
