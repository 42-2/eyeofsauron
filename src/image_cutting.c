/**
 * @file image_cutting.c
 * @brief Functions to cut image into characters
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stdio.h>
#include <stdlib.h>
#include "image.h"
#include "perceptron.h"

/**
 * Converts an array of character objects into a string
 * @param chs the array of character objects
 * @param nb_char the number of characters contained in this list
 * @param exit_code a pointer whose value will contain the exit code of the
 * function
 * @return the text represented by the array of character objects
 */
char *chs_to_string(character *chs, size_t nb_char, int *exit_code )
{
  char *text;
  text = malloc((nb_char + 1) * sizeof(char));

  if (text == NULL)
    {
      *exit_code = 12;
      return NULL;
    }

  for (size_t i = 0; i < nb_char; ++i)
    {
      text[i] = chs[i].character;
    }

  text[nb_char] = '\0';
  *exit_code = 0;
  return text;
}

/**
 * Check if a line only contains white pixels
 * @param img the image to check
 * @param line the line to check
 * @return 1 if blank, 0 otherwise, and -1 if there was an error
 */
int is_line_blank(image img, size_t line)
{
  if (line >= img.height)
    {
      return -1;
    }

  /* Checking the value of each pixel of the line */
  for (size_t i = 0; i < img.width; i++)
    {
      if (img.pixels[line][i].r == 0)
        {
          return 0;
        }
    }

  return 1;
}

/**
 * Insert a character into an array of character objects
 * @param chs the array of character objects
 * @param nb_char a pointer to the number of character objects in the array
 * @param ch the character object to insert
 * @param pos the position where the new character should be
 */
void insert_character(character *chs, size_t *nb_char, character ch,
                      size_t pos)
{
  for (size_t i = *nb_char - 2; i >= pos; --i)
    {
      chs[i + 1] = chs[i];
    }

  chs[pos] = ch;
  ++(*nb_char);
}

/**
 * Cuts the image into lines
 * @param image structure to cut
 * @param nb_lines a pointer to the number of char_line_position objects
 * @param exit_code a pointer whose value will be the exit code of the funtion
 * @return a array of char_line_position containing the first and last pixel
 * lines for each character line
 */
char_line_position *cut_lines(image img, size_t *nb_lines, int *exit_code)
{
  char_line_position *char_lines;
  char_line_position char_line;
  size_t first_pixel_line;
  char in_line = 0;
  *nb_lines = 0;
  char_lines = calloc(img.height, sizeof(char_line_position));

  if (char_lines == NULL)
    {
      *exit_code = 12;
      return char_lines;
    }

  for (size_t i = 0; i < img.height; i++)
    {
      /* If the line isn't blank, we entered a line of chs */
      if (!is_line_blank(img, i) && !in_line)
        {
          in_line = 1;
          first_pixel_line = i;
        }
      /* If the line is blank and we were in a line, we exited the line */
      else if (is_line_blank(img, i) && in_line)
        {
          in_line = 0;
          char_line.first_pixel_line = first_pixel_line;
          char_line.last_pixel_line = i - 1;
          char_lines[*nb_lines] = char_line;
          (*nb_lines)++;
        }
      /* If we reached the end of the file */
      else if (i == img.height - 1 && !is_line_blank(img, i) && in_line)
        {
          char_line.first_pixel_line = first_pixel_line;
          char_line.last_pixel_line = i;
          char_lines[*nb_lines] = char_line;
          (*nb_lines)++;
        }
    }

  if (*nb_lines == 0)
    {
      *exit_code = 42;
    }
  else
    {
      *exit_code = 0;
    }

  return char_lines;
}

/**
 * Cuts each line of characters into characters
 * @param img the image we are working on
 * @param char_lines the array of lines to cut
 * @param nb_lines the number of lines in said array
 * @param chs the array of character objects
 * @param nb_char a pointer to the number of character in said array
 * @param exit_code a pointer whose value will be the exit code of the funtion
 * @return an array of character objects
 */
character *cut_chs(image img, char_line_position *char_lines, size_t nb_lines,
                   character *chs, size_t *nb_char, int *exit_code)
{
  character ch;
  size_t first_pixel_column = 0;
  char inAChar = 0;
  *nb_char = 0;
  chs = calloc(img.width * nb_lines, sizeof(character));

  if (chs == NULL)
    {
      *exit_code = 12;
      return chs;
    }

  for (size_t i = 0; i < nb_lines; ++i)
    {
      /*
       * First, let's find every character
       */
      for (size_t j = 0; j < img.width; ++j)
        {
          for (size_t k = char_lines[i].first_pixel_line;
               k < char_lines[i].last_pixel_line; ++k)
            {
              /* if the column isn't blank, we entered a character */
              if (img.pixels[k][j].r != 255)
                {
                  if (!inAChar)
                    {
                      inAChar = 1;
                      first_pixel_column = j;
                    }

                  break;
                }
              /* If the column is blank and we were in a character, we exited
                 it */
              else if (k == char_lines[i].last_pixel_line - 1 &&
                       inAChar)
                {
                  inAChar = 0;
                  ch.first_pixel_line =
                    char_lines[i].first_pixel_line;
                  ch.last_pixel_line =
                    char_lines[i].last_pixel_line;
                  ch.first_pixel_column = first_pixel_column;
                  ch.last_pixel_column = j - 1;
                  ch.character = '\0';
                  chs[*nb_char] = ch;
                  ++(*nb_char);
                }
            }
        }

      ch.first_pixel_line = 0;
      ch.last_pixel_line = 0;
      ch.first_pixel_column = 0;
      ch.last_pixel_column = 0;
      ch.character = '\n';
      chs[*nb_char] = ch;
      ++(*nb_char);
    }

  if (*nb_char == 0)
    {
      *exit_code = 42;
    }
  else
    {
      *exit_code = 0;
    }

  return chs;
}

/**
 * Separates two characters that might have been mistaken for one
 * @param chs the array of character objects to analyze
 * @param nb_char a pointer to the number of character objects in said array
 * @return the array of character objects
 */
character *unmerge_chs(character *chs, size_t *nb_char)
{
  double char_size_average;
  size_t middle_pos;
  character new_ch;

  for (size_t i = 0; i < *nb_char; ++i)
    {
      /* The character is a ' ' or a '\n', thus we don't process it */
      if (chs[i].character != '\0')
        {
          continue;
        }

      char_size_average += (chs[i].last_pixel_column
                            - chs[i].first_pixel_column + 1);
    }

  char_size_average = char_size_average / *nb_char;

  for (size_t i = 0; i < *nb_char; ++i)
    {
      if (chs[i].last_pixel_column - chs[i].first_pixel_column + 1 >= 1.90 *
          char_size_average)
        {
          middle_pos = (size_t)((chs[i].first_pixel_column +
                                 chs[i].last_pixel_column + 0.0) / 2);
          new_ch.first_pixel_line = chs[i].first_pixel_line;
          new_ch.last_pixel_line = chs[i].last_pixel_line;
          new_ch.first_pixel_column = middle_pos;
          new_ch.last_pixel_column = chs[i].last_pixel_column;
          chs[i].last_pixel_column = middle_pos;
          new_ch.character = '\0';
          insert_character(chs, nb_char, new_ch, ++i);
        }
    }

  return chs;
}

/**
 * Insert spaces in a line of characters
 * @param chs the array of character objects
 * @param nb_char a pointer to the number of character objects in said array
 * @line_beginning where the lines in which we want to insert spaces starts in
 * the arrayy
 * @line_end where said line ends
 * @space_average the average size for a space character in this line
 */
void insert_spaces(character *chs, size_t *nb_char, size_t line_beginning,
                   size_t line_end, size_t space_average)
{
  character space_ch;

  for (size_t i = line_beginning + 1; i < line_end; ++i)
    {
      if (chs[i].first_pixel_column - chs[i - 1].last_pixel_column >= 1.4 *
          space_average)
        {
          space_ch.first_pixel_line = chs[i].first_pixel_line;
          space_ch.last_pixel_line = chs[i].last_pixel_line;
          space_ch.first_pixel_column = chs[i - 1].last_pixel_column + 1;
          space_ch.last_pixel_column = chs[i].first_pixel_column - 1;
          space_ch.character = ' ';
          insert_character(chs, nb_char, space_ch, i++);
        }
    }
}

/**
 * Finds the average size of a space in a line and then insert spaces where
 * they belong
 * @param chs the array of character objects
 * @param nb_char a pointer to the number of character objects in said array
 * @return the array of character objects
 */
character *find_spaces(character *chs, size_t *nb_char)
{
  size_t line_beginning = 0;
  double space_average = 0;

  for (size_t i = 1; i < *nb_char; ++i)
    {
      /*
       * We arrived at the end of a line
       * Now affecting character list, and start over
       */
      if (chs[i].character == '\n')
        {
          space_average = space_average / (i - line_beginning);
          insert_spaces(chs, nb_char, line_beginning, i - 1, (size_t) space_average);
          space_average = 0;
          line_beginning = i + 1;
        }
      /*
       * We just calculate the average space between two characters
       */
      else
        {
          space_average += (chs[i].first_pixel_column - chs[i - 1].last_pixel_column);
        }
    }

  return chs;
}

/**
 * Create a CHARACTER_SIZE * CHARACTER_SIZE matrix representing the character
 * by 0s and 1s. 0 being where it is blank, 1 where it is black
 * @param img the image we are processing
 * @param ch the character from which we want to create the matrix
 * @return the character
 */
character create_character_matrix(image img, character ch)
{
  for (size_t i = 0; i < INPUT_NUMBER; ++i)
    {
      ch.character_matrix[i] = 0;
    }

  /*
   * We do not need to shrink or expand the character for
   * it to fit in the CHARACTER_SIZE * CHARACTER_SIZE matrix
   */
  if (ch.last_pixel_line - ch.first_pixel_line + 1 == CHARACTER_SIZE
      && ch.last_pixel_column - ch.first_pixel_column + 1 == CHARACTER_SIZE)
    {
      for (size_t i = ch.first_pixel_line; i <= ch.last_pixel_line; ++i)
        {
          for (size_t j = ch.first_pixel_column; j <= ch.last_pixel_column;
               ++j)
            {
              ch.character_matrix[(i - ch.first_pixel_line)
                                  * CHARACTER_SIZE + j - ch.first_pixel_column]
                =
                  (img.pixels[i][j].b == MIN_PIXEL_VAL ? 1 : 0);
            }
        }
    }
  /*
   * We need to shrink or expand the character for it to fit
   */
  else
    {
      /* Credit for this shrinking algorithm goes to:
       * Mark Ransom: https://stackoverflow.com/users/5987/mark-ransom
       * And the algorithm comes from:
       * https://stackoverflow.com/questions/9570895
       */
      double xscale = (CHARACTER_SIZE + 0.0) / (ch.last_pixel_column -
                      ch.first_pixel_column + 1);
      double yscale = (CHARACTER_SIZE + 0.0) / (ch.last_pixel_line -
                      ch.first_pixel_line + 1);
      double threshold = 0.5 / (xscale * yscale);
      double xstart = 0.0;
      double xend = 0.0;
      double xportion = 1.0;
      double ystart = 0.0;
      double yend = 0.0;
      double yportion = 1.0;
      double sum = 0.0;

      for (size_t i = 0; i < CHARACTER_SIZE; ++i)
        {
          ystart = yend;
          yend = (i + 1) / yscale;

          if (yend >= CHARACTER_SIZE)
            {
              yend = CHARACTER_SIZE - 0.000001;
            }

          xend = 0.0;

          for (size_t j = 0; j < CHARACTER_SIZE; ++j)
            {
              xstart = xend;
              xend = (j + 1) / xscale;

              if (xend >= CHARACTER_SIZE)
                {
                  xend = CHARACTER_SIZE - 0.000001;
                }

              sum = 0.0;

              for (size_t y = (size_t) ystart; y <= (size_t) yend; ++y)
                {
                  yportion = 1.0;

                  if (y == (size_t) ystart)
                    {
                      yportion -= ystart - y;
                    }

                  if (y == (size_t) yend)
                    {
                      yportion -= y + 1 - yend;
                    }

                  for (size_t x = (size_t) xstart; x <= (size_t) xend; ++x)
                    {
                      xportion = 1.0;

                      if (x == (size_t) xstart)
                        {
                          xportion -= xstart - x;
                        }

                      if (x == (size_t) xend)
                        {
                          xportion -= x + 1 - xend;
                        }

                      sum += (img.pixels[ch.first_pixel_line +
                                         y][ch.first_pixel_column + x].b ==
                              MIN_PIXEL_VAL ? 1 : 0) * yportion * xportion;
                    }
                }

              ch.character_matrix[i * CHARACTER_SIZE + j] =
                sum > threshold ? 1 : 0;
            }
        }
    }

  return ch;
}

/**
 * Wrapper around create_chs_matrx to create those matrices for a whole array
 * of character objects
 * @param img te image we are processing
 * @param chs the array of character objects
 * @param nb_char the number of character objects in said array
 */
void create_chs_matrices(image img, character *chs, size_t nb_char)
{
  for (size_t i = 0; i < nb_char; ++i)
    {
      if (chs[i].character != '\0')
        {
          continue;
        }

      chs[i] = create_character_matrix(img, chs[i]);
    }
}

/**
 * Cut an image into characters
 * @param img the image to process
 * @param chs an array of character objects
 * @param nb_char a pointer whose value is the number of elements in said array
 * @param verbose whether to be verbose
 * @param exit_code a pointer whose value will be the exit code of the function
 */
character *cut_image(image img, character *chs, size_t *nb_char, int verbose,
                     int *exit_code)
{
  if (verbose)
    {
      printf("\nFinding characters in image");
    }

  *exit_code = 0;
  char_line_position *lines;
  size_t nb_lines;
  lines = cut_lines(img, &nb_lines, exit_code);

  if (*exit_code != 0)
    {
      free(lines);
      return chs;
    }

  chs = cut_chs(img, lines, nb_lines, chs, nb_char, exit_code);

  if (*exit_code != 0)
    {
      free(lines);
      return chs;
    }

  free(lines);

  if (*nb_char > 5)
    {
      chs = unmerge_chs(chs, nb_char);
      chs = find_spaces(chs, nb_char);
    }

  create_chs_matrices(img, chs, *nb_char);

  if (verbose)
    {
      printf("\nFound all characters in image\n");
    }

  return chs;
}
