/**
 * @file preprocessing.c
 * @brief Perform multiple tasks to improve the image that will be OCR'ed
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#include <stdio.h>
#include "image.h"

/**
 * Truncate a pixel value if it exceeds MAX_PIXEL_VAL or MIN_PIXEL_VAL
 * @param value the value to truncate
 * @return the truncated value
 */
unsigned char truncate(int value)
{
  if (value < MIN_PIXEL_VAL)
    {
      return MIN_PIXEL_VAL; // 0
    }

  if (value > MAX_PIXEL_VAL)
    {
      return MAX_PIXEL_VAL; // 255
    }

  return (unsigned char) value;
}

/**
 * Turn an image black and white
 * @param img an pointer to the image to modify
 */
void black_white(image *img)
{
  unsigned int r, g, b;
  unsigned char average;

  for (size_t i = 0; i < img->height; i++)
    {
      for (size_t j = 0; j < img->width; j++)
        {
          r = img->pixels[i][j].r;
          g = img->pixels[i][j].g;
          b = img->pixels[i][j].b;
          /* Calculating average pixel value */
          average = truncate((r + b + g) / 3);

          /* Setting pixels to 0 or 255 depending on average's value */
          if (average >= MID_PIXEL_VAL) // 140
            {
              img->pixels[i][j].r = MAX_PIXEL_VAL; // 255
              img->pixels[i][j].g = MAX_PIXEL_VAL;
              img->pixels[i][j].b = MAX_PIXEL_VAL;
            }
          else
            {
              img->pixels[i][j].r = MIN_PIXEL_VAL; // 0
              img->pixels[i][j].g = MIN_PIXEL_VAL;
              img->pixels[i][j].b = MIN_PIXEL_VAL;
            }
        }
    }
}

/**
 * Turn an image in shades of gray
 * @param img a pointer to the image to modify
 */
void grey_scale(image *img)
{
  unsigned int r, g, b;
  unsigned char newPixelValue;

  for (size_t i = 0; i < img->height; i++)
    {
      for (size_t j = 0; j < img->width; j++)
        {
          r = img->pixels[i][j].r;
          g = img->pixels[i][j].g;
          b = img->pixels[i][j].b;
          /* Calculating the average of the pixel */
          newPixelValue = truncate((r + g + b) / 3);
          /* Setting all component at the average value */
          img->pixels[i][j].r = newPixelValue;
          img->pixels[i][j].g = newPixelValue;
          img->pixels[i][j].b = newPixelValue;
        }
    }
}

/**
 * Preprocess the image before OCR
 * Perform multiple tasks to improve the image that will be OCR'ed
 * @param img a pointer to the image to preprocess
 * @param verbose whether to be verbose
 */
void preprocess(image *img, int verbose)
{
  if (verbose)
    {
      printf("\nPreprocessing the image");
    }

  grey_scale(img);
  black_white(img);

  if (verbose)
    {
      printf("\nSuccessfully preprocessed the image\n");
    }
}
