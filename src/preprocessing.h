/**
 * @file preprocessing.h
 * @brief Header file for preprocessing.c
 * @author 42/2
 * @version 0.21
 * @date 2018-11-02
 */
#ifndef PREPROCESSING_H
#define PREPROCESSING_H

void preprocess(image *img, int verbose);

#endif
