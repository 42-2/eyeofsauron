# Makefile

# List of executables which needs 755
EXEC = eyeofsauron
DATA_NEURAL = ${EXEC}.w
DATA_GUI = ${EXEC}.glade
RAP_STN_1 = rap-stn-1
RAP_PROJET = rap-projet
# Prefix for installation
PREFIX = /usr

# Default removal program with its options
RM = rm -rvf

.PHONY: all alldocs docs check_dependencies install uninstall format clean distclean

all: ${EXEC}

alldocs: docs doxydocs

${EXEC}: check_dependencies
	${MAKE} ${EXEC} --directory=src
	cp src/${EXEC} .
	cp src/${DATA_NEURAL} .
	cp src/${DATA_GUI} .

doxydocs:
	doxygen

docs: ${RAP_STN_1}.pdf ${RAP_PROJET}.pdf

${RAP_STN_1}.pdf: docs/img/* docs/logos/* docs/${RAP_STN_1}.tex
	${MAKE} ${RAP_STN_1}.pdf --directory=docs
	cp docs/${RAP_STN_1}.pdf .

${RAP_PROJET}.pdf: docs/img/* docs/logos/* docs/${RAP_PROJET}.tex
	${MAKE} ${RAP_PROJET}.pdf --directory=docs
	cp docs/${RAP_PROJET}.pdf .

check_dependencies:
	@echo "SDL version:"
	@sdl-config --version && echo "SDL installed, proceeding" || (echo "SDL not installed, stopping" && exit 1)
	@echo "Gtk+ version"
	@gtk-launch --version && echo "Gtk+ installed, proceeding" || (echo "Gtk+ not installed, stopping" && exit 1)

install: check_dependencies ${EXEC}
	mkdir -p /usr/share/${EXEC}
	@${RM} ${PREFIX}/share/${EXEC}/*
	@${RM} ${PREFIX}/bin/${EXEC}
	install -m 0755 ${EXEC} ${PREFIX}/share/${EXEC}
	install -m 0644 ${DATA_NEURAL} ${PREFIX}/share/${EXEC}
	install -m 0644 ${DATA_GUI} ${PREFIX}/share/${EXEC}
	ln -s ${PREFIX}/share/${EXEC}/${EXEC} ${PREFIX}/bin/${EXEC}

uninstall:
	@${RM} ${PREFIX}/share/${EXEC}
	@${RM} ${PREFIX}/bin/${EXEC}

format:
	${MAKE} format --directory=src
	${MAKE} format --directory=docs
	@for f in $(shell ls .astylerc AUTHORS Doxyfile .gitignore .gitlab-ci.yml LICENSE.md Makefile README.md); do sed -i 's/[[:space:]]*$$//' $$f; done

clean:
	${MAKE} clean --directory=src
	${MAKE} clean --directory=docs
	@${RM} defense.*

distclean: clean
	${MAKE} distclean --directory=src
	${MAKE} distclean --directory=docs
	@${RM} doxydocs
	@${RM} ${EXEC} ${DATA_NEURAL} ${DATA_GUI}
	@${RM} ${RAP_STN_1}.pdf ${RAP_PROJET}.pdf

# END
